package com.tcwgq.model;

import org.springframework.stereotype.Component;

/**
 * @author tcwgq
 * @time 2017年8月17日下午8:20:22
 * @email tcwgq@outlook.com
 */
@Component(value = "user") //相当于<bean id="user" class=""/>
public class User {
	private String name;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String toString() {
		return "User [name=" + name + "]";
	}

}
