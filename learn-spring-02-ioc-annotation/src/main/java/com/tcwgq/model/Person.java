package com.tcwgq.model;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

/**
 * Created by lenovo on 2017/8/17.
 */
@Service(value = "person")
@Scope(value = "prototype")
public class Person {
	private String name;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

}
