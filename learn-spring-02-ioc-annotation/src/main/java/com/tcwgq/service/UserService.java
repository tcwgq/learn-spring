package com.tcwgq.service;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.tcwgq.dao.UserDao;

/**
 * @author tcwgq
 * @time 2017年8月17日下午10:08:56
 * @email tcwgq@outlook.com
 */
@Service("userService")
public class UserService {
	// 使用注解时不需要set方法
	// @Autowired
	// private UserDao userDao;

	@Resource(name = "userDao123")
	private UserDao userDao;

	public void add() {
		userDao.add();
	}
}
