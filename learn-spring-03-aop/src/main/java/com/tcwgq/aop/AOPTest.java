package com.tcwgq.aop;

import com.tcwgq.model.User;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * Created by lenovo on 2017/8/20.
 */
public class AOPTest {
    @Test
    public void test(){
        ApplicationContext ctx = new ClassPathXmlApplicationContext("classpath:applicationContext.xml");
        User user = (User)ctx.getBean("user");
        user.add();
    }
}
