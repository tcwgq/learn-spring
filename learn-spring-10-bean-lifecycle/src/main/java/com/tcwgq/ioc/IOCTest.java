package com.tcwgq.ioc;

import com.tcwgq.model.User;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * @author tcwgq
 * @time 2017年8月13日下午10:27:42
 * @email tcwgq@outlook.com
 */
public class IOCTest {
	public static void main(String[] args) {
		ApplicationContext ctx = new ClassPathXmlApplicationContext(new String[] { "classpath:applicationContext.xml" });
		User user = ctx.getBean("user", User.class);
		user.add();
		System.out.println(user);
		((ClassPathXmlApplicationContext) ctx).registerShutdownHook();
	}
}
