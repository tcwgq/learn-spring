package com.tcwgq.model;

/**
 * @author tcwgq
 * @time 2017年8月14日上午8:23:18
 * @email tcwgq@outlook.com
 */
public class Student {
	private String name;

	public Student() {
		super();
	}

	public Student(String name) {
		super();
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void add() {
		System.out.println("student add...");
	}

	@Override
	public String toString() {
		return "Student [name=" + name + "]";
	}

}
