package com.tcwgq.model;

/**
 * @author tcwgq
 * @time 2017年8月13日下午10:25:22
 * @email tcwgq@outlook.com
 */
public class User {
	private String username;

	public User() {
		super();
	}

	public User(String username) {
		super();
		this.username = username;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public void add() {
		System.out.println("add...");
	}

	public void init() {
		System.out.println("init...");
	}

	public void destroy() {
		System.out.println("destroy...");
	}

	@Override
	public String toString() {
		return "User [username=" + username + "]";
	}

}
