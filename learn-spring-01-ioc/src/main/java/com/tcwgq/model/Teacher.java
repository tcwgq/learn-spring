package com.tcwgq.model;

/**
 * @author tcwgq
 * @time 2017年8月14日上午8:29:59
 * @email tcwgq@outlook.com
 */
public class Teacher {
	private String name;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

}
