package com.tcwgq.ioc;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.tcwgq.model.Teacher;

/**
 * @author tcwgq
 * @time 2017年8月14日上午8:33:53
 * @email tcwgq@outlook.com
 */
public class IOCTest2 {
	public static void main(String[] args) {
		ApplicationContext ctx = new ClassPathXmlApplicationContext("classpath:applicationContext.xml");
		Teacher teacher = (Teacher) ctx.getBean("teacher");
		System.out.println(teacher);
	}
}
