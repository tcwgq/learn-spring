package com.tcwgq.ioc;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.tcwgq.service.UserService;

/**
 * @author tcwgq
 * @time 2017年8月15日上午8:30:34
 * @email tcwgq@outlook.com
 */
public class IOCTest3 {
	public static void main(String[] args) {
		ApplicationContext ctx = new ClassPathXmlApplicationContext("classpath:applicationContext.xml");
		UserService service = (UserService) ctx.getBean("userService");
		service.add();
	}
}
