package com.tcwgq.action;

import com.opensymphony.xwork2.ActionSupport;
import com.tcwgq.service.UserService;

/**
 * @author tcwgq
 * @time 2017年8月20日上午11:16:23
 * @email tcwgq@outlook.com
 */
public class UserAction extends ActionSupport {

	private UserService userService;

	public UserAction() {

	}

	public void setUserService(UserService userService) {
		this.userService = userService;
	}

	public UserService getUserService() {
		return userService;
	}

	@Override
	public String execute() throws Exception {
		// 1.
		// ApplicationContext ctx = new
		// ClassPathXmlApplicationContext("classpath:applicationContext.xml");
		// UserService userService = (UserService) ctx.getBean("userService");
		// //2.
		// WebApplicationContext webApplicationContext =
		// ContextLoader.getCurrentWebApplicationContext();
		// UserService userService = (UserService)
		// webApplicationContext.getBean("userService");
		userService.add();
		return NONE;
	}
}
