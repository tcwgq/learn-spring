package com.tcwgq.service;

import com.tcwgq.dao.UserDao;

/**
 * @author tcwgq
 * @time 2017年8月24日下午10:13:33
 * @email tcwgq@outlook.com
 */
public class UserService {
	private UserDao userDao;

	public void setUserDao(UserDao userDao) {
		this.userDao = userDao;
	}

	public int add(int id, String username, String password) {
		return userDao.add(id, username, password);
	}
}
