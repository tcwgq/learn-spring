package com.tcwgq.dao;

import org.springframework.jdbc.core.JdbcTemplate;

/**
 * @author tcwgq
 * @time 2017年8月24日下午10:13:26
 * @email tcwgq@outlook.com
 */
public class UserDao {
	private JdbcTemplate template;

	public void setTemplate(JdbcTemplate template) {
		this.template = template;
	}

	public int add(int id, String username, String password) {
		System.out.println("UserDao add...");
		String sql = "insert into t_user values(?, ?, ?)";
		return template.update(sql, id, username, password);
	}

	public int update(int id, int money) {
		String sql = "update t_user set money = money + ? where id = ?";
		return template.update(sql, money, id);
	}
}
