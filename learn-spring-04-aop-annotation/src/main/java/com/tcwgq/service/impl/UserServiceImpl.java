package com.tcwgq.service.impl;

import com.tcwgq.model.User;
import com.tcwgq.service.UserService;
import org.springframework.stereotype.Service;

@Service
public class UserServiceImpl implements UserService {
    @Override
    public User add(User user) {
        System.out.println("UserService.add(user), user=" + user);
        return user;
    }
}
